// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MyHomework3 : ModuleRules
{
	public MyHomework3(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
