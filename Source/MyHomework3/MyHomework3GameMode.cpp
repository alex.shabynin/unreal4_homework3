// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyHomework3GameMode.h"
#include "MyHomework3Character.h"
#include "UObject/ConstructorHelpers.h"

AMyHomework3GameMode::AMyHomework3GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
